# iosacal-mybinder

a demo repository for interactive usage of iosacal with mybinder.org

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fcodeberg.org%2Fsteko%2Fiosacal-mybinder/HEAD?filepath=iosacal-demo.ipynb)